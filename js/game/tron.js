    
    var square, blnError;
    var kaart;
    var timer;

    square = 5;
    blnError = false;

    $(document).keydown(function(e) {
        for (var i = 0; i < kaart.players.length; i++) {
            var player = kaart.players[i];

            if (isValidObject(player)) {
                switch (e.keyCode) {
                case player.actionKeys.up:
                    if (player.lastAction != "down") {
                        player.lastAction = "up";
                    }
                    return false;
                    break;
                case player.actionKeys.down:
                    if (player.lastAction != "up") {
                        player.lastAction = "down";
                    }
                    return false;
                    break;
                case player.actionKeys.left:
                    if (player.lastAction != "right") {
                        player.lastAction = "left";
                    }
                    return false;
                    break;
                case player.actionKeys.right:
                    if (player.lastAction != "left") {
                        player.lastAction = "right";
                    }
                    return false;
                    break;
                }
            }
        }
    });

    $(document).ready(function() {
        //console.clear();
        timer = $.timer(timerTick, 50);
        timer.stop();
        createMap();
        startGame();

    });

	function timerTick()
	{
		if (kaart.players.length > 0)
		{
	        for (var i = 0; i < kaart.players.length; i++) {
	            var _player = kaart.players[i];
		            
		        movePlayer(_player);    
	            collisionDetection(_player);
	            createBlockPlayer(_player);
	        }
	        checkCrash();
       }
    };

    function startGame() {
        var p1 = "tom"; //document.getElementById("player1_name").value;
        var p2 = "jens"; //document.getElementById("player2_name").value;
        if (p1.length >= 0) {
            kaart.addPlayer(p1);
        }
        if (p2.length >= 0) {
            kaart.addPlayer(p2);
        }
        timer.set({ time : 50, autostart : true });
    }

    function createBlockPlayer(_player) {
        var newCanvas = $('<div/>', {
            id: 'canvas' + _player.id
        });

        newCanvas.css("position", "absolute");
        newCanvas.css("background-color", _player.color);
        newCanvas.css("width", square + "px");
        newCanvas.css("height", square + "px");
        newCanvas.css("left", _player.x + "px");
        newCanvas.css("top", _player.y + "px");
        $('#field').append(newCanvas);
        
        _player.arrLine.push(_player.x + "," + _player.y);
		_player.id += 1;
    }
    
    function createCloud(_player) {
        var newCloud = $('<div/>', {
            id: 'cloud' + _player.id
        });
        var img = $('<img id="dynamic">');
		img.css("max-width", "20px");
		
        newCloud.css("position", "absolute");
        newCloud.css("left", _player.x - 7 + "px");
        newCloud.css("top", _player.y - 5 + "px");
        
        img.attr('src', "media\\cloud.gif");
        img.appendTo(newCloud);
        
        $('#field').append(newCloud);
    }

    function collisionDetection(_player) 
    {
        borderCollision(_player);
        playerCollision(_player);        
    }
    
    function playerCollision(_player)
    {
        for (var o = 0; o <= kaart.players.length-1; o++) 
        {
            var p2 = kaart.players[o];
    		if (jQuery.inArray(_player.x + "," + _player.y, p2.arrLine) >= 0)
        	{
                setCrash(_player);
            }
        }
    }
    
    function movePlayer(_player)
    {
    	switch (_player.lastAction) 
        {
            case "left":
                _player.x -= square;
               	break;
            case "up":
                _player.y -= square;
                break;
            case "right":
                _player.x += square;
                break;
            case "down":
                _player.y += square;
        		break;
    	}
    }
    
    function borderCollision(_player)
    {
    	switch (_player.lastAction) 
        {
            case "left":
                if (_player.x < square) 
               	{
                    setCrash(_player);
                }
                break;
            case "up":
                if (_player.y < square) 
                {
                    setCrash(_player);
                }
                break;
            case "right":
                if (_player.x > kaart.width)
                {
                    setCrash(_player);
                }
                break;
            case "down":
                if (_player.y > kaart.height)
                {
                    setCrash(_player);
            	}
        		break;
    	}
    }
    
    function checkCrash()
    {
    	var strCrash = [];
    	var amount;
    	amount = 0;
    	for (var i = 0; i < kaart.players.length; i++) {
            var _player = kaart.players[i];
	        
	        if(_player.objCrash.crashed)
	        {
	        	amount++;
	        	strCrash[amount-1] = _player.name;
	        	createCloud(_player);
	            timer.stop();
	        }
        }
        if(amount > 0)
        {
        	var strEndScreen;
        	
        	if(amount = 1)
        	{
        		strEndScreen = "Loser is " + strCrash[amount-1];
        	}
        	else if(amount < kaar.player.length())
        	{
        		strEndScreen = "Losers are ";
        		for(var i = 0;i<amount;i++){
        			strEndScreen = strEndScreen + strCrash[i] + " and ";
        		}
        		strEndScreen = strEndScreen.substring(0,(strEndScreen.length - 5));
        	}
        	else{
        		strEndScreen = "DRAW";
        	}
        	createEndScreen(strEndScreen);
        }
    }
    
    function createEndScreen(_strCrash)
    {
    	$('#error').html =  $('#error').html(_strCrash);
		$('#error').css("position","absolute");
		$('#error').css("background-color","#FFFFFF");
		$('#error').css("padding","10px");
		$('#error').css({"border-color": "#000000", 
						   "border-width":"2px", 
						   "border-style":"solid"});
		$('#error').css("width","200px");
		$('#error').css("height","100px");
		var position = $('#field').position();
		
		top =  kaart.height / 2;
		left = kaart.width / 2;
		
		$('#error').css("top", top + "px");
		$('#error').css("left", left + "px");
		$('#error').css("z-index", "1");
    }
    
    function setCrash(_player)
    {
    	_player.objCrash.crashed = true;
        _player.objCrash.location[0] = _player.x;
        _player.objCrash.location[1] = _player.y;
    }
    
    function createMap()
    {
    	kaart = new map();
    	kaart.setWidth($(window).innerWidth());
    	kaart.setHeight($(window).innerHeight());
    	kaart.setColor("#688389");
    }
