function map()
{
	this.width = 0;
	this.height = 0;
	this.color = "";
	
  	this.players = [];
  	this.square = 5;
  	this.usedColors = [];
  	
}

map.prototype.setWidth = function(_width){
	this.width = _width - 20;
    $('#field').css("width", this.width + "px");
};

map.prototype.setHeight = function(_height){
	this.height = _height - 20;
    $('#field').css("height", this.height + "px");
};

map.prototype.setColor = function(_color){
	this.color = _color;
	$('#field').css("background-color",_color);
};

map.prototype.addPlayer = function(name){
	var actionsKeys;	
	var startDirection;
	var x;
	var y;
	
	switch(this.players.length){
		case 0:
			actionsKeys = new actionKeys(90, 83, 81, 68);
			startDirection = "right";
			x = 0 - square;
			y = this.height / 2; /*$(window).innerHeight() / 2;*/
			break;
		case 1:
			actionsKeys = new actionKeys(38, 40, 37, 39);
			startDirection = "left";
			x = this.width; //$(window).width() - 20;
			y = this.height / 2; //$(window).height() / 2;
			break;
	}
	
	var newPlayer = new player(name, "", x, y, actionsKeys, startDirection);
	newPlayer.notSoRandomColor();
	this.players.push(newPlayer);
};

