require(["vendor/angularJS/angular"], function(util) {
	console.log("angular loaded");
	require(["vendor/jquery/jquery"], function(util) {
		console.log("jquery loaded");
		require(["vendor/jquery/jquery.timer"], function(util) {
			console.log("jquery timer loaded");
			require(["global/globalFunctions"], function(util) {
				console.log("globalFUnctions loaded");
				require(["game/player"], function(util) {
					console.log("player loaded");
					require(["game/map"], function(util) {
						console.log("map loaded");
						require(["game/tron"], function(util) {
							console.log("tron loaded");
						});
					});
				});
			});
		});
	});
}); 