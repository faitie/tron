/**
 * @author Faitie
 */

//Global functions used troughout the game that are not bound to actual game classes

//Check if a object is valid
function isValidObject(objToTest) {
  if (null === objToTest) return false;
  if ("undefined" == typeof(objToTest)) return false;
  return true;
}