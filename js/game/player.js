
function player(_name, _color, _startPositionX, _startPositionY, _actionKeys, _action)
{
  this.name = _name;
  this.color = _color;
  
  this.x = _startPositionX;
  this.y = _startPositionY;
  this.lastAction = _action;
  
  this.id = 0;
  
  this.actionKeys = _actionKeys;
  this.arrLine = [];
  this.objCrash = new objCrash();
}

player.prototype.toString=function(){ 
	return "naam : " + this.name;
};

player.prototype.randomColor = function(){
  var blnOk = false;
  
  while(!blnOk){
	this.color = '#' + Math.floor(Math.random() * 16777215).toString(16);
	if(jQuery.inArray(this.color, kaart.usedColors) == -1){blnOk = true;}
  }
  
  kaart.usedColors.push(this.color);
};

player.prototype.notSoRandomColor = function(){
  var blnOk = false;
  
  if(jQuery.inArray('#FF0000', kaart.usedColors) == -1)
  {
  	this.color = '#FF0000';
  }
  else
  {
  	this.color = '#0000FF';
  }
  
  kaart.usedColors.push(this.color);
};

function actionKeys(keyUp, keyDown, keyLeft, keyRight)
{
  this.up = keyUp;
  this.down = keyDown;
  this.left = keyLeft;
  this.right = keyRight;
}

function objCrash()
{
    this.crashed = false;
    this.location = [0,0];
}